#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "mpi.h"

int** crearMatriz(int dimension);
int *obtenerDiagonalPrincipal(int** matriz, int dimension);
int *obtenerDiagonalSecundaria(int** matriz, int dimension);
int obtenerValorMasAlto(int* diagonalP, int dimension);
int sumarDiagonal(int* diagonal, int proceso, int filas);
int valorMayor(int n1, int n2);
int diagonalMasGrande(int n1, int n2);

int main(int argc, char *argv[]){

    int dimension = 10000;

    int** matriz = crearMatriz(dimension);
    int* diagonalP;
    int* diagonalS;

    int proceso, nro_procesos;

    int sumaDiagonalP = 0;
    int totalDP = 0;
    int sumaDiagonalS = 0;
    int totalDS = 0;
    int valor1 = 0;
    int valor2 = 0;
    int valorMasAlto = 0;
    int diagonalGrande = 0;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &proceso);
    MPI_Comm_size(MPI_COMM_WORLD, &nro_procesos);
    int dimensionParcial = dimension/nro_procesos;
    

    diagonalP = obtenerDiagonalPrincipal(matriz, dimension);
    sumaDiagonalP = sumarDiagonal(diagonalP, proceso, dimensionParcial);
    valor1 = obtenerValorMasAlto(diagonalP, dimension);

    diagonalS = obtenerDiagonalSecundaria(matriz, dimension);
    sumaDiagonalS = sumarDiagonal(diagonalS, proceso, dimensionParcial);
    valor2 = obtenerValorMasAlto(diagonalS, dimension);
        
    MPI_Reduce(&sumaDiagonalP, &totalDP, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&sumaDiagonalS, &totalDS, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (proceso == 0)
    {
        printf("SUMA DE LA DIAGONAL PRINCIPAL: %d\n", totalDP);
        printf("SUMA DE DIAGONAL SECUNDARIA: %d\n", totalDS);
        printf("VALOR MAS ALTO DE LA DIAGONAL PRINCIPAL: %d\n", valor1);
        printf("VALOR MAS ALTO DE LA DIAGONAL SECUNDARIA: %d\n", valor2);
        valorMasAlto = valorMayor(valor1, valor2);
        diagonalGrande = diagonalMasGrande(totalDP, totalDS);
         if (diagonalGrande == 1)
         {
            printf("LA DIAGONAL PRINCIPAL ES LA MAS GRANDE\n");
         }else if (diagonalGrande == 0)
         {
            printf("LA DIAGONAL SECUNDARIA ES LA MAS GRANDE\n");
         }
    }

    free(matriz);

    MPI_Finalize();

}

int** crearMatriz(int dimension) {
    int **matriz;
    int i;

    matriz = (int**)malloc(dimension * sizeof(int*)); // asignar memoria para las filas
    for(i = 0; i < dimension; i++) {
        matriz[i] = (int*)malloc(dimension * sizeof(int)); // asignar memoria para las columnas
    }

    for (int i = 0; i < dimension; i++)
    {
        for (int j = 0; j < dimension; j++)
        {
            matriz[i][j] = rand() % 1250;
        }
    }

    return matriz;
}

int *obtenerDiagonalPrincipal(int** matriz, int dimension){
    static int diagonalP[10000];
    int contador = 0;

    for (int i = 0; i < dimension; i++) {
        diagonalP[i] = matriz[i][i];
        contador++;
    }
   return diagonalP;
}

int *obtenerDiagonalSecundaria(int** matriz, int dimension){
    static int diagonalS[10000];
    int contador = 0;
    int i, j;
    for (i = dimension-1, j = 0; i >= 0 && j < dimension; i--, j++) {
        diagonalS[j] = matriz[i][j];
    }
   return diagonalS;
}

int sumarDiagonal(int* diagonal, int proceso, int filas){
    int suma = 0;
    int inicio = filas * proceso;
    int fin = filas * (proceso + 1);

    for (int i = inicio; i < fin; i++)
    {
        suma += diagonal[i];
    }

    return suma;
}

int obtenerValorMasAlto(int* diagonal, int dimension){
    int max;

    max = diagonal[0];
    for (int i = 1; i < dimension; i++) {
        if (diagonal[i] > max) {
            max = diagonal[i];
        }
    }
    return max;
}

int valorMayor(int n1, int n2){

    if(n1 > n2){
        printf("LA DIAGONAL PRINCIPAL POSEE EL VALOR MAS ALTO: %d \n", n1);
        return n1;
    }else{
        printf("LA DIAGONAL SECUNDARIA POSEE EL VALOR MAS ALTO: %d\n", n2);
        return n2;
    }
}

int diagonalMasGrande(int n1, int n2){

    if(n1 > n2){
        return 1;
    }else{
        return 0;
    }
}
