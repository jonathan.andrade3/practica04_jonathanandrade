#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"

void ingresarNumero(int proceso, int *numeroBuscar);
int** crearMatriz(int dimension);
int numeroCoincidencias(int** matriz, int numero, int proceso, int dimension, int filas);

int main(int argc, char *argv[]){

    int dimension = 10000;

    int** matriz = crearMatriz(dimension);

    int proceso, nro_procesos;

    int contadorCoincidencias = 0;
    int coincidenciasTotales = 0;
    int numeroBuscar = 0;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &proceso);
    MPI_Comm_size(MPI_COMM_WORLD, &nro_procesos);

    int dimensionParcial = dimension/nro_procesos;

    ingresarNumero(proceso, &numeroBuscar);

    contadorCoincidencias = numeroCoincidencias(matriz, numeroBuscar, proceso, dimension, dimensionParcial);
    printf("Numero coincidencias en el proceso %d = %d\n", proceso, contadorCoincidencias);

    MPI_Reduce(&contadorCoincidencias, &coincidenciasTotales, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (proceso == 0)
    {
        printf("Número Total de Coincidencias: %d\n", coincidenciasTotales);
    }

    free(matriz);

    MPI_Finalize();

}

void ingresarNumero(int proceso, int *numeroBuscar)
{
    if (proceso == 0)
    {
        printf("Ingresa un número entre 0-100 para realizar la búsqueda\n");
        scanf("%d", numeroBuscar);
    }

    MPI_Bcast(numeroBuscar, 1, MPI_INT, 0, MPI_COMM_WORLD);
}

int** crearMatriz(int dimension) {
    int **matriz;
    int i;

    matriz = (int**)malloc(dimension * sizeof(int*)); // asignar memoria para las filas
    for(i = 0; i < dimension; i++) {
        matriz[i] = (int*)malloc(dimension * sizeof(int)); // asignar memoria para las columnas
    }

    for (int i = 0; i < dimension; i++)
    {
        for (int j = 0; j < dimension; j++)
        {
            matriz[i][j] = rand() % 100;
        }
    }
    
    return matriz;
}

int numeroCoincidencias(int** matriz, int numero, int proceso, int dimension, int filas){
    int contador = 0;
    int inicio = filas * proceso;
    int fin = filas * (proceso + 1);

    for (int i = inicio; i < fin; i++)
    {
        for (int j = 0; j < dimension; j++)
        {   
            if (matriz[i][j] == numero)
            {
                contador++;
            }
        }
    }

    return contador;

}